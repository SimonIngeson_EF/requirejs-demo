﻿using System.Web.Optimization;

namespace RequireJsTest.WebApp
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.AddScript("~/bundles/js",
                "~/Scripts/require.js",
                "~/Scripts/require-config.js");

            bundles.AddStyle("~/bundles/css",
                "~/Content/Site.css");

            BundleTable.EnableOptimizations = true;
        }
    }

    public static class BundleCollectionExtensions
    {
        public static void AddScript(this BundleCollection bundles, string virtualPath, params string[] includes)
        {
            var bundle = new ScriptBundle(virtualPath).Include(includes);
            bundles.Add(bundle);
        }

        public static void AddStyle(this BundleCollection bundles, string virtualPath, params string[] includes)
        {
            var bundle = new StyleBundle(virtualPath).Include(includes);
            bundles.Add(bundle);
        }
    }
}
