﻿using System.ComponentModel.DataAnnotations;

namespace RequireJsTest.WebApp.Models.Home
{
    public class PersonViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}