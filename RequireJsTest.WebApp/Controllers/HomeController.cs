﻿using System.Web.Mvc;
using RequireJsTest.WebApp.Models.Home;

namespace RequireJsTest.WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Form()
        {
            return View(new PersonViewModel());
        }

        [HttpPost]
        public ActionResult Form(PersonViewModel person)
        {
            return View(person);
        }
    }
}