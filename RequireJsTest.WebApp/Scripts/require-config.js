﻿/*global requirejs: false */
requirejs.config({
    baseUrl: "/Scripts",
    paths: {
        "bootstrap": "bootstrap",
        "jquery": "jquery-2.1.1",
        "jquery.validate": "jquery.validate",
        "jquery.validate.unobtrusive": "jquery.validate.unobtrusive",
        "knockout": "knockout-3.2.0"
    },
    shim: {
        "bootstrap": {
            deps: ["jquery"]
        },
        "jquery.validate": {
            deps: ["jquery"]
        },
        "jquery.validate.unobtrusive": {
            deps: ["jquery", "jquery.validate"]
        }
    }
});
