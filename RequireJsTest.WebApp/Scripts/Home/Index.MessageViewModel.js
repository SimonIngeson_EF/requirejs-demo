﻿/*global define: false */
define(["knockout"], function (ko) {
    "use strict";
    
    return function (model) {
        return {
            IsVisible: ko.observable(model.show || true),
            Message: ko.observable(model.message || '')
        };
    };
});
