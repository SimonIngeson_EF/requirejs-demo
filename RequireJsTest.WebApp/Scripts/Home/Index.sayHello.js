﻿/*global define: false */
define(function () {
    "use strict";

    function sayHello(name) {
        name = name || "World";
        return "Hello " + name + "!";
    }

    return sayHello;
});
