﻿/*global define: false */
define(["knockout", "Home/Index.sayHello", "Home/Index.MessageViewModel"], function (ko, sayHello, messageViewModel) {
    "use strict";

    function validateOptions(options) {
        if (options.rootNode === undefined) {
            throw new Error("rootNode was not set");
        }
        if (options.rootNode.nodeType !== 1) {
            throw new Error("rootNode was not a valid DOM element");
        }
    }

    function main(options) {
        validateOptions(options);
        var viewModel = messageViewModel({ message: sayHello(options.name) });
        ko.applyBindings(viewModel, options.rootNode);
    }

    return main;
});
